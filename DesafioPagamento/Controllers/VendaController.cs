using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DesafioPagamento.Models;
using Microsoft.AspNetCore.Mvc;

namespace DesafioPagamento.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private static List<Vendedor> vendedores()
        {
            return new List<Vendedor>{
                new Vendedor{Id = 1,Nome="vendedor1", Cpf = 123456789, Email="vendedor1@gmail", Telefone = 54789658},

                new Vendedor{Id = 2,Nome="vendedor2", Cpf = 2569985, Email="vendedor2@gmail", Telefone = 5488888}
            };
        }

        private static List<Venda> vendas()
        {
            return new List<Venda>{
                new Venda{IdPedido = 1,IdVendedor=1 ,Data =DateTime.Now, ItemPedido ="2 vassouras e 1 rodo", 
                Status= 0 },

                new Venda{IdPedido = 2,IdVendedor=2 ,Data =DateTime.Now, ItemPedido ="2 rodos e 1 pazinha"}
            };
        }
        


        [HttpGet("Obter Vendedores")]
        public IActionResult MostrarVendedores()
        {
         return Ok(vendedores());
        }

        [HttpGet("Obter Vendas")]
        public IActionResult MostrarVendas()
        {
         return Ok(vendas());
        }

        [HttpPost("CriarVenda")]
        public IActionResult CriarVenda(Venda venda)
        {         

           return Ok(vendas(venda));
        }

        private static List<Venda> vendas(Venda venda)
        {
              return new List<Venda> {new Venda {IdPedido = venda.IdPedido,IdVendedor=venda.IdVendedor ,Data =DateTime.Now, ItemPedido =venda.ItemPedido}};
        }
    }
}