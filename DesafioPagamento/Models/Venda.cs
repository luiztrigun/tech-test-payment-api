using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioPagamento.Models
{
    public class Venda
    {
        // public Venda(int idVendedor, DateTime data, int idPedido, string itemPedido)
        // {
        //     IdVendedor = idVendedor;
        //     Data = data;
        //     IdPedido = idPedido;
        //     ItemPedido = itemPedido;
        //    // Status = status;
        // }

        public int IdVendedor { get; set; }
        public DateTime Data { get; set; }
        public int IdPedido { get; set; }
        public string ItemPedido { get; set; }
       public EnumStatusVenda Status { get; set; }
    }
}